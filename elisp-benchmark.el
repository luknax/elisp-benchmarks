;;; elisp-benchmark.el ---  -*- lexical-binding:t -*-

;; Copyright (C) 2019 Andrea Corallo

;; Maintainer: akrl@sdf.org
;; Package: elisp-benchmark
;; Homepage: https://gitlab.com/koral/elisp-benchmark
;; Version: 0.2
;; Package-Requires: ((emacs "27") (cl-lib "0.5"))
;; Keywords: languages, extensions, verilog, hardware, rtl

;; This file is not part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:
;; In use for testing the Emacs Lisp native compiler performance.

;;; Usage:
;; emacs -batch -l ~/elisp-benchmark/elisp-benchmark.el --eval '(elb-run)'

;;; Code:

(require 'cl-lib)
(require 'comp)

(defconst elb-bench-folder
  (concat (file-name-directory (or load-file-name buffer-file-name))
	  "benchmarks/"))

(defconst elb-benchmarks '(;; ("pidigits.el" . elb-pidigits-entry)
			   ("nbody.el". elb-nbody-entry)
			   ("bubble.el" . elb-bubble-entry)
			   ("bubble-no-cons.el" . elb-bubble-no-cons-entry)
			   ("fibn.el" . elb-fibn-entry)
			   ("fibn-rec.el" . elb-fibn-rec-entry)
			   ("fibn-tco.el" . elb-fibn-tco-entry)
			   ("inclist-tco.el" . elb-inclist-tco-entry)
			   ("listlen-tco.el" . elb-listlen-tco-entry)))

(defmacro elb-time (&rest body)
  "Measure and return the time it takes to evaluate BODY."
  (declare (doc-string 2) (indent 1))
  (let ((t-sym (gensym)))
    `(let ((,t-sym (current-time)))
       ,@body
       (float-time (time-since ,t-sym)))))

(defun elb-run ()
  "Run all the benchmarks."
  (cl-loop with comp-speed = 3
	   for (file . entry-point) in elb-benchmarks
	   for abs-file = (concat elb-bench-folder file)
	   for byte-t = (progn
			  (cl-assert
			   (and
			    (byte-compile-file abs-file)
			    (load (concat abs-file "c"))))
			  (message "Running %s byte compiled..." file)
			  (elb-time
			      (message "res: %s" (funcall entry-point))))
	   for native-t = (progn
			    (cl-assert
			     (and
			      (native-compile abs-file)
			      (load (concat abs-file "n"))))
			    (message "Running %s native compiled..." file)
			    (elb-time
				(message "res: %s" (funcall entry-point))))
	   do (message "%s orig time: %fs native time: %fs boost %f%%" file
		       byte-t native-t
		       (* 100 (1- (/ byte-t native-t))))))

;;; elisp-benchmark.el ends here
