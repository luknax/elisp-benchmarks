;; -*- lexical-binding: t; -*-
;; Adapted to elisp from CL version from:
;; https://drmeister.wordpress.com/2015/07/30/timing-data-comparing-cclasp-to-c-sbcl-and-python/

(defun elb-fibn (reps num)
  (let ((z 0))
    (dotimes (_ reps)
      (let ((p1 1)
            (p2 1))
        (dotimes (_ (- num 2))
          (setf z (+ p1 p2)
                p2 p1
                p1 z))))
    z))

(defun elb-fibn-entry ()
  ;; Use 80 to stay in the fixnum range.
  (elb-fibn 3000000 80))
