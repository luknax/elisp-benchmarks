;; -*- lexical-binding: t; -*-

(require 'cl-lib)

(defvar elb-inclist-tco-len 300)
(defvar elb-inclist-tco-list (mapcar #'random (make-list elb-inclist-tco-len
							 100)))

(defun elb-inclist-tco (l)
  (when l
    (cl-incf (car l))
    (elb-inclist-tco (cdr l))))

(defun elb-inclist-tco-entry ()
  (let ((l (copy-sequence elb-inclist-tco-list)))
    (cl-loop repeat 1000000
	     do (elb-inclist-tco l))))
