;; -*- lexical-binding: t; -*-

(require 'cl-lib)

(defun elb-fibn-tco (a b count)
  (if (= count 0)
      b
    (elb-fibn-tco (+ a b) a (- count 1))))

(defun elb-fibn-tco-entry ()
  (cl-loop repeat 1000000
	   do (elb-fibn-tco 1 0 80)))
