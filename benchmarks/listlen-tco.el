;; -*- lexical-binding: t; -*-

(require 'cl-lib)

(defvar elb-listlen-tco-len 300)
(defvar elb-listlen-tco-list (mapcar #'random (make-list elb-listlen-tco-len
							 100)))

(defun elb-listlen-tco (l n)
  (if (null l)
      n
    (cl-incf n)
    (elb-listlen-tco (cdr l) n)))

(defun elb-listlen-tco-entry ()
  (let ((l (copy-sequence elb-listlen-tco-list)))
    (cl-loop repeat 1000000
	     do (elb-listlen-tco l 0))))
