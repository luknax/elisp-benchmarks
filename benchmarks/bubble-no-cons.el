;; -*- lexical-binding: t; -*-
;; Like bubble but in place

(require 'cl-lib)

(defvar elb-bubble-len 1000)
(defvar elb-bubble-list (mapcar #'random (make-list elb-bubble-len
						    most-positive-fixnum)))
(defun elb-bubble-no-cons (list)
  (cl-loop repeat (length list)
	   do
	   (cl-loop for x on list
		    for a = (car x)
		    for b = (cadr x)
		    when (and b (> a b))
		    do (progn
			 (setcar x b)
			 (setcar (cdr x) a))
		    finally (return list))))

(defun elb-bubble-no-cons-entry ()
  (cl-loop repeat 200
	   for l = (copy-sequence elb-bubble-list)
	   do (elb-bubble-no-cons l)))

(provide 'elb-bubble)
